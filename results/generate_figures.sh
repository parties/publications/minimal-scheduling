#!/usr/bin/env bash

###############################################################################
#         FILE: generate_figures.sh
#        USAGE: ./generate_figures.sh
#  DESCRIPTION: Script to generate figures of assignments and model from the
#  .            .tex files
#       AUTHOR: Jaime Arias <arias@lipn.univ-paris13.fr>
###############################################################################

function tex-pdf {
  pdflatex -halt-on-error -interaction=nonstopmode $1 > $1.txt
  grep '^!.*' --color=always $1.txt
  rm $1.txt
}

TEX_FILE=`find . -type f  \( -iname "*.tex" ! -iname "base.tex" \)`;

for path in $TEX_FILE; do
  folder=`dirname $path`;
  filename=`basename $path .tex`;

  echo 'Processing ' $path;
  (cd $folder; tex-pdf "$filename.tex" && tex-pdf "$filename.tex" && pdftoppm -png "$filename.pdf" "$filename" && if [[ $(ls $filename*.png | wc -l) = 1 ]]; then mv "$filename-1.png" "$filename.png"; fi);
done
