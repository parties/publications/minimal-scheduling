#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import dataframe_image as dfi
import os


# ### Load `adt2maude` results

# In[ ]:


adt2maude_case_studies_df = pd.read_csv('adt2maude_results_case_studies.csv')
adt2maude_scaling_df = pd.read_csv('adt2maude_results_scaling.csv')
adt2maude_df = pd.concat([adt2maude_case_studies_df, adt2maude_scaling_df])


# ### Load `adt2amas` results

# In[ ]:


adt2amas_case_studies_df = pd.read_csv('adt2amas_results_case_studies.csv')
adt2amas_scaling_df = pd.read_csv('adt2amas_results_scaling.csv')
adt2amas_df = pd.concat([adt2amas_case_studies_df, adt2amas_scaling_df])


# ### Plot tables

# Firstly, we remove all the unnecessary columns.

# In[ ]:


adt2maude_df = adt2maude_df.drop(['command', 'median', 'stddev', 'user', 'system', 'min', 'max', 'parameter_tool'], axis=1)
adt2maude_df = adt2maude_df.set_index('parameter_model')
adt2maude_df = adt2maude_df * 1000 # convert seconds in milliseconds
adt2maude_df


# In[ ]:


adt2amas_df = adt2amas_df.drop(['command', 'median', 'stddev', 'user', 'system', 'min', 'max', 'parameter_tool'], axis=1)
adt2amas_df = adt2amas_df.set_index('parameter_model')
adt2amas_df = adt2amas_df * 1000 # convert seconds in milliseconds
adt2amas_df


# Secondly, we join both data tables

# In[ ]:


df = adt2amas_df.join(adt2maude_df, lsuffix="_adt2amas", rsuffix="_adt2maude")
df = df.reset_index()
df = df.rename(columns={"parameter_model": "model"})
df["model"] = df["model"].apply(lambda path: os.path.basename(path).split('.txt')[0])
df


# We can calculate when `adt2maude` is better than `adt2amas`.

# In[ ]:


adt2maude_is_better_df = df[df['mean_adt2amas'] > df['mean_adt2maude']]
print(f"adt2maude is {len(adt2maude_is_better_df)} times better than adt2amas")
if (len(adt2maude_is_better_df)):
    print(adt2maude_is_better_df)


# We extract the experiments where `maude` takes more than 4ms

# In[ ]:


df[df['mean_adt2maude'] > 4000]


# We parse the table using latex syntax

# In[ ]:


s = df.style.format(precision=2, escape="latex").format_index("\\textbf{{{}}}", escape="latex", axis=1).hide(axis='index')
s.to_latex("benchmark.tex", hrules=True)


# We save the table as an image

# In[ ]:


s = df.style.format(precision=2).hide(axis='index')
dfi.export(s, "benchmark.png", max_rows=-1)

