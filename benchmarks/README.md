# Benchmarks

This folder contains the scripts to reproduce the benchmarks results shown
in the paper.

## Requirements

- To run the benchmarks, we use the tool
  [hyperfine](https://github.com/sharkdp/hyperfine).

- We suppose that `adt2maude` is in the folder `tool` of this repository.
  Otherwise, please run the script `adt2maude.sh` that you can find in that
  folder.

- The script `run_experiments` in the `scalability` folder has been executed in
  order to generate all the scaling models.

- To install the python requirements you can run `pip3 install -r requirements.txt`

## Run Benchmarks

We need to run the script `run_benchmarks.sh` to generate the `csv` files that
contain the time in milliseconds consumed by the tools `adt2amas` and
`adt2maude`. The generated files are:

- `adt2amas_results_case_studies.csv`
- `adt2maude_results_case_studies.csv`
- `adt2amas_results_scaling.csv`
- `adt2maude_results_scaling.csv`

## Generate Tables

We provide a notebook (`plot_table.ipynb`) that plots the benchmarks results. We
also provide the `python` code of the notebook, that allows for plotting without
running `jupyter`, as follows:

```
python plot_table.py
```

The script will generate the files `benchmark.{tex,png}` which contain a table
comparing the time needed by `adt2amas` and `adt2maude` to compute the minimal
scheduling for each ADTree model.
