#!/usr/bin/env bash

###############################################################################
#         FILE: run_benchmarks.sh
#        USAGE: ./run_benchmarks.sh
#  DESCRIPTION: Script to run the benchmarks for adt2amas and adt2maude
#       AUTHOR: Jaime Arias <arias@lipn.univ-paris13.fr>
###############################################################################

# Check that hyperfine is installed
if ! command -v hyperfine &> /dev/null ; then
    echo "hyperfine is not installed"
    exit 1
fi

# Path to adt2maude source code
ADT2MAUDE_PATH='../tool/adt2maude'

# Path to the adt2amas binary
ADT2AMAS_PATH=''
case $(uname -s) in
   Linux) ADT2AMAS_PATH='../tool/adt2amas-linux';;
  Darwin) ADT2AMAS_PATH='../tool/adt2amas-macos';;
  Windows) ADT2AMAS_PATH='../tool/adt2amas.exe' ;;
      *) echo "OS is not supported by adt2amas"
         exit 1 ;;
esac

# Path to the case studies ADTrees
CASE_STUDIES_FOLDER="../results"
case_studies=`find "${CASE_STUDIES_FOLDER}" -name '*.txt' -type f | tr " " ,`
case_studies=`echo $case_studies | tr " " ,`

# Path to the scalability ADTrees
SCALING_MODELS_FOLDER="../scalability"
scaling_models=`find "${SCALING_MODELS_FOLDER}" -name '*_AND' -type f | tr " " ,`
scaling_models=`echo $scaling_models | tr " " ,`

# -----------------------------------------------------------------------------
# ADT2AMAS benchmarks
# -----------------------------------------------------------------------------

  # Run benchmarks for adt2amas for case studies
hyperfine \
  --warmup 3 \
  --runs 10 \
  --parameter-list model ${case_studies} \
  --parameter-list tool ${ADT2AMAS_PATH} \
  --export-csv "adt2amas_results_case_studies.csv" \
  '{tool} minimal --model {model}'

  # Run benchmarks for adt2amas for scalability models
hyperfine \
  --warmup 3 \
  --runs 10 \
  --parameter-list model ${scaling_models} \
  --parameter-list tool ${ADT2AMAS_PATH} \
  --export-csv "adt2amas_results_scaling.csv" \
  '{tool} minimal --model {model}'

# -----------------------------------------------------------------------------
# ADT2MAUDE benchmarks
# -----------------------------------------------------------------------------

# Run benchmarks for adt2maude for case studies
hyperfine \
  --warmup 3 \
  --runs 10 \
  --parameter-list model ${case_studies} \
  --parameter-list tool ${ADT2MAUDE_PATH} \
  --export-csv "adt2maude_results_case_studies.csv" \
  '{tool}/min-schedule.py --input {model}'

# Run benchmarks for adt2maude for case studies
hyperfine \
  --warmup 3 \
  --runs 10 \
  --parameter-list model ${scaling_models} \
  --parameter-list tool ${ADT2MAUDE_PATH} \
  --export-csv "adt2maude_results_scaling.csv" \
  '{tool}/min-schedule.py --input {model}'