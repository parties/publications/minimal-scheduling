#!/usr/bin/env python3
# coding: utf-8

import subprocess
import pathlib
import os
import re
import sys
import csv
import itertools


def get_tool_name():
    """Return the name of the adt2amas tool depending on the operating system"""
    tool = 'adt2amas'
    if sys.platform.startswith('linux'):
        return f'{tool}-linux'
    elif sys.platform.startswith('darwin'):
        return f'{tool}-macos'
    else:
        return f'{tool}.exe'

DEPTHS = [d for d in range(2, 6, 1)]
WIDTHS = [w for w in range(2, 11, 1)]
CHILDREN = [c for c in range(2, 11, 2)]
TYPES = ["AND"]

PATH = pathlib.Path(os.path.abspath(__file__)).parent
PROJECT_PATH = PATH.parent

generator = os.path.join(PATH, 'generator')
adt2amas = os.path.join(PROJECT_PATH, 'tool', get_tool_name())

agent_regex = r"# Agents: (\d+)"
slots_regex = r"# Slots: (\d+)"
width_regex = r"width: (\d+)"
size_regex = r"# nodes: (\d+)"
depth_regex = r"depth: (\d+)"

with open('results.csv', 'w') as csv_file:
    fieldnames = [
        'name', 'filename', 'depth', 'width', 'children', 'nodes', 'agents',
        'slots'
    ]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    for (depth, width, children,
         type_node) in itertools.product(DEPTHS, WIDTHS, CHILDREN, TYPES):
        filename = f"adtree-d{depth}_w{width}_c{children}_{type_node}"

        generator_command = f"{generator} -w {width} -d {depth} -c {children} -t {type_node} -f {filename}"
        generator_output = subprocess.check_output(generator_command,
                                                   shell=True,
                                                   encoding='utf-8')

        # get the generated adtree width, depth and size
        output_width = re.search(width_regex, generator_output)
        output_depth = re.search(depth_regex, generator_output)
        adt_size = re.search(size_regex, generator_output)
        if not (output_width and output_depth and adt_size):
            continue
        output_width = output_width.group(1)
        output_depth = output_depth.group(1)
        adt_size = adt_size.group(1)

        adt2amas_command = f"{adt2amas} minimal --model {filename}"
        output = subprocess.check_output(adt2amas_command,
                                         shell=True,
                                         encoding='utf-8')

        n_agents = re.search(agent_regex, output)
        n_slots = re.search(slots_regex, output)

        if not (n_agents and n_slots):
            continue

        print(adt2amas_command)
        print(output)
        writer.writerow({
            'depth':
            output_depth,
            'width':
            output_width,
            'children':
            children,
            'nodes':
            adt_size,
            'agents':
            n_agents.group(1),
            'slots':
            n_slots.group(1),
            'filename':
            filename,
            'name':
            f"d{output_depth}_w{output_width}_c{children}_n{adt_size}",
        })
