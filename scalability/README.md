# Scaling Experiments

This folder contains the scripts to reproduce the scalability results shown
in the paper.

## 1. Create random ADTrees models

Firstly, the ADTree models generator (`generator.c`) must be compiled. For
that, it is necessary to have installed `gcc`, and then run `make`. Now, you
can run the `run_experiments` script that will generate several ADTree
models, randomly. It only requires `python3`.

```
./run_experiments
```

The script will output the file `results.csv` which summarises the
information of all generated ADTree models with the minimal scheduling
computed by our tool (`adt2amas`). That is, their _depth_, _width_,
_# children_, _# nodes_, _# agents_, and _# slots_.

## 2. Plot

We provide the notebook (`plot_results.ipynb`) that plots the scalability
results. In order to run the notebook, some dependencies must be installed
with `pip`, as follows:

```
pip install -r requirements.txt
```

We also provide the `python` code of the notebook, that allows for plotting
without running `jupyter`, as follows:

```
python plot_results.py
```

The scripts will generate the figure (`scalability.{pdf, png}`) comparing the
minimal scheduling computed for each generated ADTree model (`results.csv`
file). In addition, these results are parsed as a table in `latex` format
(`scaling_table.tex`).

## 3. Generation of a ADTree models

We provide the tool `generator`. This tool generates ADTree models with the
following specifications.

```
./generator -d <depth> -w <width> -c <children> -t <type> -f <output_file>
```

where:

- `depth` is the depth of the ADTree.
- `width` is the number of deepmost leaves.
- `children` corresponds to the number of children of each gate.
- `type` corresponds to the type of the gates of the ADTree. It can be `OR` or `AND`.
- `output_file` is the path where the output will be saved.
