#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import pandas as pd
import plotly.io as pio
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# render figures in notebook
pio.renderers.default = "notebook_connected"

# templates figures
px.defaults.template = "simple_white"
pio.templates.default = "simple_white"


# # Load Data

# In[2]:


# csv file with the output
csv_file = os.path.join("results.csv")

# read data
original_df = pd.read_csv(csv_file)

# remove duplicates
df = original_df.drop_duplicates(subset=['name']).copy()
df = df.sort_values(by=['depth', 'width', 'children'])
df = df.reset_index(drop=True)
df.head()


# In[4]:


# configurations where the number of agents does not correspond to the width
df_special = df[df.width != df.agents]
df_special


# # Generate Figures

# In[5]:


# colors & fonts
dark_grey = 'rgb(55, 83, 109)'
blue = 'rgb(26, 118, 255)'
tickfont = dict(size=10)
axisfont = dict(size=12)

# create figure
fig = make_subplots(rows=2, cols=1, vertical_spacing=0.15)

# add agents figure
colors = ['crimson' if i in df_special.index else dark_grey for i in range(df.size)]
fig.add_trace(
    go.Bar(x=df['name'], y=df['agents'], name="# Agents", text=df['agents'], marker_color=colors),
    row=1, col=1
)

# add slots figure
fig.add_trace(
    go.Bar(x=df['name'], y=df['slots'], name="# Slots", text=df['slots'], marker_color=blue),
    row=2, col=1
)

# Set x-axes 
fig.update_xaxes(title_text="<b>ADTree Configuration</b>", row=1, col=1, tickangle=-90, tickfont=tickfont, title_font=axisfont, title_standoff = 2, autorange= True)
fig.update_xaxes(title_text="<b>ADTree Configuration</b>", row=2, col=1, tickangle=-90, tickfont=tickfont, title_font=axisfont, title_standoff = 2, autorange= True)

# Set y-axes
fig.update_yaxes(title_text="<b># Agents</b>", row=1, col=1, rangemode="tozero", tickmode = 'linear', dtick = 1, tickfont=tickfont, title_font=axisfont, title_standoff = 12, autorange= True)
fig.update_yaxes(title_text="<b># Slots</b>", row=2, col=1, rangemode="tozero", tickmode = 'linear', dtick = 1, tickfont=tickfont, title_font=axisfont, title_standoff = 12, autorange= True)

# update layout
fig.update_traces(textposition='outside')
fig.update_layout(showlegend=False, height=860, width=900, margin=dict(l=1, r=1, t=1, b=1))

# show figure
fig.show()


# In[6]:


# saving figures
fig.write_image("scalability.png")
fig.write_image("scalability.pdf")


# # Export to LaTeX

# In[7]:


with open('scaling_table.tex', 'w', encoding="UTF-8") as f:
    latex_table = df.to_latex(index=False, bold_rows=True, multirow=True, 
                              columns=['depth', 'width', 'children', 'nodes', 'agents', 'slots'],
                              header=['depth', 'width', '# children', '|ADTree|', '# agents', '# slots'],
                              column_format="|r|r|r|r||r|r|",
                              caption="Scalability of different agent configurations",
                              label="scalability")
    f.write(latex_table)
    print(latex_table)


# In[ ]:




